﻿namespace Ecs.Types
{
    public enum ComponentType
    {
        Position = 0,
        Rotation = 1,
        View = 2,
        Input = 3,
        Name = 4
    }
}