﻿using Ecs.Types;

namespace Ecs.Contracts
{
    public interface IComponent
    {
        ComponentType Type { get; }
        ulong Mask { get; }
    }
}