﻿using System.Collections.Generic;

namespace Ecs.Contracts
{
    public interface IWorld
    {
        IList<IEntity> Entities { get; }
    }
}