﻿using System.Collections;
using System.Collections.Generic;
using Ecs.Types;

namespace Ecs.Contracts
{
    public interface IEntity
    {
        ulong ComponentsMask { get; }
        IList<IComponent> Components { get; }

        void HasComponent(ComponentType componentType);
        void GetComponent(ComponentType componentType);
        void SetComponent(IComponent component);
        void RemoveComponent(ComponentType componentType);
    }
}