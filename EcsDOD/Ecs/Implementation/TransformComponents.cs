﻿using System;
using Ecs.Mocks;

namespace Ecs
{
    public struct TransformComponents : ITransformComponents
    {
        public bool[] IsAlive { get; private set; }
        public bool[] IsActive { get; private set; }
        public bool[] IsDirty { get; private set; }
        public Vector2D[] Position { get; private set; }
        public Quaternion[] Rotation { get; private set; }

        public TransformComponents(int size)
        {
            IsAlive = new bool[size];
            IsActive = new bool[size];
            IsDirty = new bool[size];
            Position = new Vector2D[size];
            Rotation = new Quaternion[size];
        }
    }
}

