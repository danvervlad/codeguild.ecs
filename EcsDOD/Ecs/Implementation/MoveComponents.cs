﻿using System;
using Ecs.Mocks;

namespace Ecs
{
    public struct MoveComponents : IMoveComponents
    {
        public bool[] IsAlive { get; private set; }
        public bool[] IsActive { get; private set; }
        public bool[] IsDirty { get; private set; }
        public Vector2D[] TargetPosition { get; private set; }
        public Quaternion[] TargetRotation { get; private set; }

        public MoveComponents(int size)
        {
            IsAlive = new bool[size];
            IsActive = new bool[size];
            IsDirty = new bool[size];
            TargetPosition = new Vector2D[size];
            TargetRotation = new Quaternion[size];
        }
    }
}