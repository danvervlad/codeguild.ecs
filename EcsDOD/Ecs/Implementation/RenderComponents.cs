using System;
using Ecs.Mocks;

namespace Ecs
{
    public struct RenderComponents : IRenderComponents
    {
        public bool[] IsAlive { get; private set; }
        public bool[] IsActive { get; private set; }
        public bool[] IsDirty { get; private set; }
        public Sprite[] Sprite { get; private set; }

        public RenderComponents(int size)
        {
            IsAlive = new bool[size];
            IsActive = new bool[size];
            IsDirty = new bool[size];
            Sprite = new Sprite[size];
        }
    }
}