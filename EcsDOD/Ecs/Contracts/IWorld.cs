﻿using System;
using System.Collections.Generic;

namespace Ecs
{
    public interface IWorld
    {
        IList<IBaseComponents> Components { get; }
        bool[] Entities { get; }

        IEntityManager IEntityManager { get; }
    }
}

