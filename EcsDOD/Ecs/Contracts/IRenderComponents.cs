using System;
using Ecs.Mocks;

namespace Ecs
{
    public interface IRenderComponents : IBaseComponents
    {
        Sprite[] Sprite { get; }
    }
}