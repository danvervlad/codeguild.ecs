using System;
using Ecs.Mocks;

namespace Ecs
{
    public interface ITransformComponents : IBaseComponents
    {
        Vector2D[] Position { get; }
        Quaternion[] Rotation { get; }
    }
}