using System;
using Ecs.Mocks;

namespace Ecs
{
    public interface IBaseComponents
    {
        bool[] IsAlive { get; }
        bool[] IsActive { get; }
        bool[] IsDirty { get; }
    }
}