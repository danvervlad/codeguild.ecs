﻿using System;

namespace Ecs
{
    public interface IEntityManager
    {
        int CreateEntity(IWorld world);
        void RemoveEntity(int entityIndex, IWorld world);

        void AddComponent();
        void UpdateComponent();
        void RemoveComponent();
    }
}

