using System;
using Ecs.Mocks;

namespace Ecs
{
    public interface IMoveComponents : IBaseComponents
    {
        Vector2D[] TargetPosition { get; }
        Quaternion[] TargetRotation { get; }
    }
}